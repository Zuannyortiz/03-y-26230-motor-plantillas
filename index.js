const express = require("express");
const app = express();
const hbs = require("hbs");
require("dotenv").config();
const router = require('./routes/public');

app.use("/", router);

app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set("views", __dirname + "/views")
hbs.registerPartials(__dirname + "/views/partials");

const  puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log(`Servidor corriendo en http://localhost:${puerto}`);
});

